/************************************************************************
File: LCD.h
Author: Emanuel Oberholzer
Description: Driver for an alpha-numeric LCD Display
Restrictions:
        Microprocessor ATMega644P
        Only support for 4-bit interface
************************************************************************/

#ifndef LCD_H
#define LCD_H

#include "HtlStddef.h"

void LCDInit(void);

void LCDWrite(char* aBuffer, unsigned char aSize);

void LCDWriteChar(unsigned char aCharacter);

void LCDClear(void);

/**
 * Function: LCDSetCursor
 *
 * Description:
 *	Set the posisiton of the cursor to the specified values.
 *
 * Parameters:
 *  aColumn	-  The Column starting with 1
 *	aRow	-	The Line of the display starting with 1
 *
 * Return:
 *	ETRUE if the position can be set, else EFALSE
 */
TBool LCDSetCursor(unsigned char aColumn, unsigned char aRow);

#endif
