
#include "LCD.h"
#include "HtlStddef.h"
#include <avr/io.h>
#include <stdio.h>

#define LENGTH_OF(x) (sizeof(x) / sizeof(x[0]))

int putLCD(char character, FILE* file);

int main() {
  LCDInit();

  fdevopen(putLCD, NULL);

  char data[] = "Hello world";
  LCDWrite(data, LENGTH_OF(data));
  printf("DIC is Super");
  while (1) {
  }
}

int putLCD(char character, FILE* file) {
  LCDWriteChar(character);
  return 1;
}
