/************************************************************************
File: LCD.c
Author: Emanuel Oberholzer
Description: Driver for an alpha-numeric LCD Display
Restrictions:
Microprocessor ATMega644P
Only support for 4-bit interface
************************************************************************/

#include "LCD.h"
#include "HtlStddef.h"
#include "LcdConstants.h"

/******************** Private Function Declarations ********************/

/**
 * Function: _LCDOut4Bit
 *
 * Description:
 *	_LCDOut4Bit writes the upper nibble ( bit 4 to bit 7) out to the
 *	data lines. One enable impulse triggers the transfer.
 *
 * Parameters:
 *	aByte - The upper 4 bits are written to the data lines.
 */
static void _LCDOut4Bit(unsigned char aByte);

/**
 * Function: _LCDEnable
 */
static void _LCDEnable(void);

/**
 * Function: _LCDCommand
 *
 * Description:
 *	Send a command to the LCD-Display
 *
 * Parameters:
 *	aCommand -
 */
static void _LCDCommand(unsigned char aCommand);

/**
 * Function: _LCDCheckBusy
 *
 * Description:
 *	Check the busy flag in the status register.
 */
static void _LCDCheckBusy(void);

/**
 * Function: _LCDReadBusyFlag
 *
 * Description:
 *	Read the busy flag from the status register.
 */
static TBool _LCDReadBusyFlag(void);

/**
 * Function: _LCDReadStatusRegister
 *
 * Description:
 *	Read the status register.
 */
static unsigned char _LCDReadStatusRegister(void);

/**
 * Function: _LCDRead4
 *
 * Description:
 *	Read 4 bit, stored in the lower nibble.
 */
static unsigned char _LCDRead4(void);

/******************** Public Function Definitions ********************/

void LCDInit(void) {
  LCD_PORT_DATA &= ~(0x0F << LCD_DB);
  LCD_DDR_DATA |= (0x0F << LCD_DB);

  LCD_PORT_RS &= ~(0x0F << LCD_RS);
  LCD_DDR_RS |= (0x0F << LCD_RS);

  LCD_PORT_RW &= ~(0x0F << LCD_RW);
  LCD_DDR_RW |= (0x0F << LCD_RW);

  LCD_PORT_EN &= ~(0x0F << LCD_EN);
  LCD_DDR_EN |= (0x0F << LCD_EN);

  // Software reset 3 times
  _LCDOut4Bit(LCD_SOFT_RESET);
  DelayMs(LCD_SOFT_RESET_MS1);
  _LCDOut4Bit(LCD_SOFT_RESET);
  DelayMs(LCD_SOFT_RESET_MS2);
  _LCDOut4Bit(LCD_SOFT_RESET);
  DelayMs(LCD_SOFT_RESET_MS3);

  // Set to 4 Bit mode
  _LCDOut4Bit(LCD_SET_FUNCTION | LCD_FUNCTION_4BIT);

  DelayMs(LCD_SET_4BITMODE_MS);

  // Initialize 2 lines, 5x7 Matrix
  _LCDCommand(LCD_SET_FUNCTION | LCD_FUNCTION_4BIT | LCD_FUNCTION_2LINE |
              LCD_FUNCTION_5X7);

  // Initialize the cursor
  _LCDCommand(LCD_SET_DISPLAY | LCD_DISPLAY_ON | LCD_CURSOR_OFF |
              LCD_BLINKING_OFF);

  // Cursor increment, no scroll
  _LCDCommand(LCD_SET_ENTRY | LCD_ENTRY_INCREASE | LCD_ENTRY_NOSHIFT);

  LCDClear();
}

void LCDWrite(char* aBuffer, unsigned char aSize) {
  unsigned char i;
  for (i = 0; i < aSize; i++)
    LCDWriteChar(aBuffer[i]);
}

void LCDWriteChar(unsigned char aCharacter) {
  _LCDCheckBusy();
  LCD_PORT_RS |= (1 << LCD_RS);
  LCD_PORT_RW &= ~(1 << LCD_RW);
  _LCDOut4Bit(aCharacter);
  _LCDOut4Bit(aCharacter << 4);
}

void LCDClear(void) {
  _LCDCommand(LCD_CLEAR_DISPLAY);
  DelayMs(LCD_CLEAR_DISPLAY_MS);
}

TBool LCDSetCursor(unsigned char aColumn, unsigned char aRow) {
  unsigned char addr;

  if (aColumn > 0x14)
    return EFALSE;

  switch (aRow) {
    case 1:
      addr = aColumn + LCD_DDADR_LINE1 - 1;
      break;
    case 2:
      addr = aColumn + LCD_DDADR_LINE2 - 1;
      break;
    case 3:
      addr = aColumn + LCD_DDADR_LINE3 - 1;
      break;
    case 4:
      addr = aColumn + LCD_DDADR_LINE4 - 1;
      break;
    default:
      return EFALSE;
  }

  _LCDCommand(addr | LCD_SET_DDADR);

  return ETRUE;
}

/******************** Private Function Definitions ********************/

static void _LCDOut4Bit(unsigned char aByte) {
  LCD_DDR_DATA |= (0x0F << LCD_DB);
  LCD_PORT_RW &= ~(1 << LCD_RW);
  LCD_PORT_DATA &= ~(0x0F << LCD_DB);
  LCD_PORT_DATA |= ((aByte & 0xF0) >> (4 - LCD_DB));
  _LCDEnable();
}

static void _LCDEnable(void) {
  LCD_PORT_EN |= (1 << LCD_EN);
  DelayUs(LCD_ENABLE_US);
  LCD_PORT_EN &= ~(1 << LCD_EN);
  DelayUs(LCD_ENABLE_US);
}

static void _LCDCommand(unsigned char aCommand) {
  _LCDCheckBusy();

  LCD_PORT_RS &= ~(1 << LCD_PORT_RS);
  _LCDOut4Bit(aCommand);
  _LCDOut4Bit(aCommand << 4);

  DelayUs(LCD_COMMAND_US);
}

static void _LCDCheckBusy(void) {
  unsigned int busyCounter = 0;

  while (_LCDReadBusyFlag()) {
    busyCounter++;
    if (busyCounter > LCD_MAX_BUSY_COUNTER)
      break;
    DelayUs(1);
  }
}

static TBool _LCDReadBusyFlag(void) {
  unsigned char statusRegister;

  statusRegister = _LCDReadStatusRegister();

  return ((statusRegister & 0x80) != 0);
}

static unsigned char _LCDReadStatusRegister(void) {
  unsigned char statusRegister;

  LCD_PORT_RS &= ~(1 << LCD_RS);
  statusRegister = _LCDRead4();
  statusRegister = statusRegister << 4;
  statusRegister |= (_LCDRead4() & 0x0F);

  return statusRegister;
}

static unsigned char _LCDRead4(void) {
  unsigned char tmp;

  LCD_DDR_DATA &= ~(0x0F << LCD_DB);

  LCD_PORT_RW |= (1 << LCD_RW);
  LCD_PORT_EN |= (1 << LCD_EN);
  DelayUs(LCD_ENABLE_US);
  tmp = LCD_PIN_DATA;
  LCD_PORT_EN &= ~(1 << LCD_EN);
  tmp = (tmp >> LCD_DB) & 0x0F;

  LCD_DDR_DATA |= (0x0F << LCD_DB);

  return tmp;
}
